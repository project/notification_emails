<?php

class notification_email
{
	private $delta;
	private $source;
	private $email;
	private $subject;
	private $admin_message;
	private $title;
	private $description;
	private $additional_data;
	private $use_node = true;
	private $use_user = true;

	/**
	 * Class cunstructor
	 * Needs unique id of notisication email. Uses for identify email
	 *
	 * @param string $delta
	 */
	public function __construct($delta)
	{
		$this->delta = $delta;
	}

	/**
	 * Inits object with default values
	 * Uses in hook implementations for supply default values
	 *
	 * @param string $soruce
	 * @param string $email
	 * @param string $subject
	 * @param string $admin_message
	 * @param string $title
	 * @param string $description
	 * @param array $additional_data
	 */
	public function init($soruce, $email, $subject, $admin_message, $title, $description, $use_user = true, $use_node = true, $additional_data = array())
	{
		$this->source = $source;
		$this->email = $email;
		$this->subject = $subject;
		$this->admin_message = $admin_message;
		$this->title = $title;
		$this->description = $description;
		$this->use_user = $use_user;
		$this->use_node = $use_node;
		$this->additional_data = $additional_data;
	}

	/**
	 * Loads object values from DB or default values
	 * Return true of values loaded
	 *
	 * @return true
	 */
	public function load()
	{
			if ($t = $this->get($this->delta))
			{
				foreach(get_object_vars($t) as $key => $value)
				{
					$this->$key = $value;
				}
				return true;
			}
			return false;
	}

	/**
	 * Sets dinamic email
	 * Can be used by modules for send notification to different emails
	 * If dinamic email address defined, email will be sent to this address instead of admin address from settings
	 *
	 * @param string $email
	 */
	public function set_dinamic_email($email)
	{
		$this->dinamic_email = $email;
	}

	/**
	 * Returns email delta
	 *
	 * @return string
	 */
	public function get_delta(){
		return $this->delta;
	}

	/**
	 * Returns email source (module name)
	 *
	 * @return string
	 */
	public function get_source(){
		return $this->source;
	}

	/**
	 * Returns email address
	 *
	 * @return string
	 */
	public function get_email(){
		return $this->email;
	}

	/**
	 * Returns email subject
	 *
	 * @return string
	 */
	public function get_subject(){
		return $this->subject;
	}

	/**
	 * Returns email body
	 *
	 * @return string
	 */
	public function get_body(){
		return $this->admin_message;
	}

	/**
	 * Returns notification email title for administer page
	 *
	 * @return string
	 */
	public function get_title(){
		return $this->title;
	}

	/**
	 * Returns notification email description for administer page
	 *
	 * @return string
	 */
	public function get_description(){
		return $this->description;
	}

	/**
	 * Returns array with kays of additional data which can be used in email templates
	 *
	 * @return array
	 */
	public function get_additional_data(){
		return $this->additional_data;
	}

	/**
	 * Loads notification emails from all modules, override object values with data from db
	 * If called without delta, returns all emails, with delta - only requested email
	 *
	 * @param string $delta
	 * @return mix
	 */
	private static function get($delta = null)
	{
		static $notifications;
		if (!isset($notifications))
		{
			$notifications = module_invoke_all('notification_emails');
		}

		$sql  = 'SELECT * FROM {notification_emails}';

		if ($delta != null)
		{
			$sql .= ' WHERE delta = \'%s\'';
		}

		$result = db_query($sql, $delta);
		while ($row = db_fetch_array($result))
		{
			if (is_a($notifications[$row['delta']], 'notification_email'))
			{
				$notifications[$row['delta']]->load_values($row);
			}
		}
		return $delta ? $notifications[$delta] : $notifications;
	}

	/**
	 * Overrides object vars with values from db
	 *
	 * @param array $values
	 */
	private function load_values($values)
	{
		foreach ($values as $key => $value)
		{
			$this->$key = $value;
		}
	}

	/**
	 * Returns array with all notification emails defined in system
	 *
	 * @return array
	 */
	static public function get_all()
	{
		static $emails;
		if (!isset($emails))
		{
			$emails = notification_email::get();
		}
		return $emails;
	}

	/**
	 * Sends notification email, adds supplied values in templates
	 *
	 *
	 * @param stdClass $node
	 * @param stdClass $user
	 * @param array $additional_variables
	 */
	public function send($node, $user, $additional_variables = array())
	{
		$this->load();
		$id = 'notification-email-' . $this->get_delta();
		$params = $this->get_template_parameters($user, $node, $additional_variables);
		$to = $this->dinamic_email ? $this->dinamic_email : $this->get_email();
		$subject = t($this->get_subject(), $params);
		$admin_message =  t($this->get_body(), $params);
		$from = variable_get('site_mail', ini_get('sendmail_from'));
		return drupal_mail($id, $to, $subject, $admin_message, $from, array('Content-Type' => 'text/html'));
	}

	/**
	 * Array with available template variables
	 *
	 * @param stdClass $user
	 * @param stdClass $node
	 * @param array $additional_variables
	 * @return array
	 */
	public function get_template_parameters($user = null, $node = null, $additional_variables = array())
	{
		$parameters = array();
		if ($this->use_user && $user)
		{
			foreach (get_object_vars($user) as $user_key => $user_var)
			{
				$parameters['!user_' . $user_key] = $user_var;
			}
			$parameters['!user_link'] = l($user->name, 'user/' . $user->uid, array(), null, null, true);
			unset($parameters['!user_pass']);
		}
		if ($this->use_node && $node)
		{
			foreach (get_object_vars($node) as $node_key => $node_var)
			{
				$parameters['!node_' . $node_key] = $node_var;
			}
			$parameters['!node_link'] = l($node->title, 'node/' . $node->nid, array(), null, null, true);
			unset($parameters['!node_taxonomy']);
		}
		$parameters['!date'] = format_date(time());

		foreach ((array)$this->get_additional_data() as $index => $key)
		{
			$parameters[$key] = $additional_variables[$key];
		}
		return $parameters;
	}

	/**
	 * Saves new values of changeable object vars in db
	 *
	 * @param string $email
	 * @param string $subject
	 * @param string $admin_message
	 */
	public function save($email, $subject, $admin_message)
	{
		$this->email = $email;
		$this->subject = $subject;
		$this->admin_message = $admin_message;
		db_query("DELETE FROM {notification_emails} WHERE delta = '%s'", $this->get_delta());
		$sql = 'INSERT INTO {notification_emails} (delta, source, email, subject, admin_message) VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\')';
		db_query($sql, $this->get_delta(), $this->get_source(), $this->get_email(), $this->get_subject(), $this->get_body());
	}
}
?>