
Notification Emails

The module provides API for other modules to send notification emails.
Install this module only if you use another module which depends on it.
The module provides user interface to edit email address, subject and body template using set of variables.

To install, place the entire module folder into your modules directory.

Go to Administer -> Site Building -> Modules and enable the Notification emails module.

To change notification settings go to 
Administer -> Site Configuration -> Notification emails

To integrate your module with Notification emails:

1. Implement hook_notification_emails() to define notification emails, you want to use

function example_notification_emails()
{
	$email = array();
	
	$notification = new notification_email('example_noditication_1');
	$notification->init(
		'example', // source module of notification email
		variable_get('site_mail', ini_get('sendmail_from')), //default email for notifications 
		'User !user_name has sent following forward', // default email subject
		'User: !user_link, Node: !node_link, To: !recipients', // default email body template
		'Forward Notify', // title of notification email
		'Notifies admins about content forward', // description of notification email
		true, // true if user infomation can be used in notification email
		true, // true if node information can be used in notification email
		array('!var1', '!var2') // array with other variables, available in template
	);
	 
	$email['example_noditication_1'] = $notification;
	return $email;
}

2. Send notification emails when you need to:
	$additional_data = array(
		'!var1' => $some_string, 
		'!var2' => $someother_string,
	);
	
	$notification = new notification_email('example_noditication_1');
	$notification->send($node, $user, $additional_data);
		
3. If you want to send notification email to different users, not just on one email address use dynamic email address:

	$notification = new notification_email('example_noditication_2');
	$notification->set_dinamic_email($user->mail);
	$notification->send(null, $user, $additional_data);

